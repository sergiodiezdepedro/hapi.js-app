const Hapi = require('hapi');
const mongoose = require('mongoose');


// Inicializar el servidor
const server = new Hapi.Server();

// Añadir la conexión
server.connection({
    port: 8000,
    host: 'localhost'
});


// Arrancar el servidor
server.start((err) => {
    if (err) {
        throw err;
    }

    console.log(`Servidor iniciado en : ${server.info.uri}`);
});

//Mongoose
mongoose.connect('mongodb://localhost/hapidb', {
        useMongoClient: true
    })
    .then(() => console.log('MongoDB conectada...'))
    .catch(err => console.error(err));

// Home Route
server.route({
    method: 'GET',
    path: '/',
    handler: (request, reply) => {
        reply.view('index', {
            name: 'Pablo Laso'
        });
    }
});

// GET Task Route
server.route({
    method: 'GET',
    path: '/tasks',
    handler: (request, reply) => {
        let tasks = Task.find((err, tasks) => {
            reply.view('tasks', {
                tasks:tasks
            });
        });
    }
});

// POST Task Route
server.route({
    method: 'POST',
    path: '/tasks',
    handler: (request, reply) => {
        let text = request.payload.text;
        let newTask = new Task({
            text:text
        });
        newTask.save((err, task) => {
            if(err) {
                return console.log(err);
            }
            return reply.redirect().location('tasks');
        });
    }
});

// Route Dinámica
server.route({
    method: 'GET',
    path: '/user/{name}',
    handler: (request, reply) => {
        reply(`<h1>Hola, ${request.params.name}</h1>`);
    }
});

// Routes Estáticas
server.register(require('inert'), (err) => {
    if (err) {
        throw err;
    }

    server.route({
        method: 'GET',
        path: '/about',
        handler: (request, reply) => {
            reply.file('./public/about.html');
        }
    });

    server.route({
        method: 'GET',
        path: '/image',
        handler: (request, reply) => {
            reply.file('./public/hapi.png');
        }
    });
});

// Crear Model de Tareas
const Task = mongoose.model('Task', {
    text: String}
);

// Vision Templates Plugin
server.register(require('vision'), (err) => {
    if (err) {
        throw err;
    }

    server.views({
        engines: {
            html: require('handlebars')
        },
        path: __dirname + '/views'
    });
});